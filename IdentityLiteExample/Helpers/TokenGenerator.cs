﻿using System;
using System.Security.Claims;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

namespace IdentityLiteExample.Helpers
{
    public class TokenGenerator
    {
        public string GenerateBearerToken(string username, string email, string fullname)
        {
            if (string.IsNullOrEmpty(username))
                return "failed";

            var identity = new ClaimsIdentity(Startup.OAuthBearerOptions.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, fullname));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, username));
            identity.AddClaim(new Claim(ClaimTypes.Email, email));

            var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
            var currentUtc = new SystemClock().UtcNow;
            ticket.Properties.IssuedUtc = currentUtc;
            ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromMinutes(60));
            var accessToken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);
            return accessToken;
        }
    }
}