﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Web;
using IdentityLiteExample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace IdentityLiteExample
{
    public class CustomAuthenticationManager
    {
        public void SignIn(UserDto user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            var claims = new List<Claim>
            {
                new Claim("UserName", user.UserName),
            };

            if (!string.IsNullOrEmpty(user.FullName))
            {
                claims.Add(new Claim(ClaimTypes.Name, user.FullName));
            }

            if (!string.IsNullOrEmpty(user.Email))
            {
                claims.Add(new Claim(ClaimTypes.Email, user.Email));
            }

            var identity = new ClaimsIdentity(
                claims, DefaultAuthenticationTypes.ApplicationCookie, "UserName", ClaimTypes.Role);

            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        public void LogOff()
        {
            AuthenticationManager.SignOut();
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }

    }
}
