namespace IdentityLiteExample.Models
{
    public class UserDto
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
    }
}