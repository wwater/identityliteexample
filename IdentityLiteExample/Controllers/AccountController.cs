﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IdentityLiteExample.Helpers;

namespace IdentityLiteExample.Controllers
{
    public class AccountController : Controller
    {
        private readonly TokenGenerator tokenGenerator;

        public AccountController()
        {
            this.tokenGenerator = new TokenGenerator();
        }

        public ActionResult TokenLogin()
        {
            return View("TokenLogin");
        }

        [HttpPost]
        public ActionResult TokenLogin(string userName, string password)
        {
            // validate username and password

            var fullname = "Not so random token dude";
            var email = "notsorandom@token.dude";

            var token = tokenGenerator.GenerateBearerToken(userName, email, fullname);

            return View("TokenLogin", "", token);
        }

        public ActionResult AngularLogin()
        {
            return View();
        }
    }
}