﻿using System.Web.Mvc;

namespace IdentityLiteExample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}