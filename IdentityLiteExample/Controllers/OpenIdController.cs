﻿using System.Web.Mvc;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.OpenId.RelyingParty;
using IdentityLiteExample.Models;

namespace IdentityLiteExample.Controllers
{
    public class OpenIdController : Controller
    {
        private readonly CustomAuthenticationManager authenticationManager;

        public OpenIdController()
        {
            this.authenticationManager = new CustomAuthenticationManager();
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            var openid = new OpenIdRelyingParty();
            var response = openid.GetResponse();
            if (response == null)
            {
                try
                {
                    var authenticationRequest = openid.CreateRequest(Startup.OpenIdUrl);

                    var fields = new ClaimsRequest();
                    fields.FullName = DemandLevel.Require;
                    fields.Email = DemandLevel.Require;
                    authenticationRequest.AddExtension(fields);

                    return authenticationRequest.RedirectingResponse.AsActionResultMvc5();
                }
                catch (ProtocolException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View("Login");
                }

            }
            else
            {
                switch (response.Status)
                {
                    case AuthenticationStatus.Authenticated:
                        var userName = response.FriendlyIdentifierForDisplay;
                        var email = "";
                        var fullName = "";

                        var claims = response.GetExtension<ClaimsResponse>();
                        if (claims != null)
                        {
                            email = claims.Email;
                            fullName = claims.FullName;
                        }

                        var user = new UserDto()
                        {
                            UserName = userName,
                            Email = email,
                            FullName = fullName
                        };
                        authenticationManager.SignIn(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    case AuthenticationStatus.Canceled:
                        ModelState.AddModelError("", "Canceled at open id provider");
                        return View("Login");
                    case AuthenticationStatus.Failed:
                        ModelState.AddModelError("", response.Exception.Message);
                        return View("Login");
                }
            }
            return new EmptyResult();
        }

        public ActionResult LogOff()
        {
            authenticationManager.LogOff();
            var openIdLogOffUrl = Startup.OpenIdUrl + "Account/LogOff";
            return Redirect(openIdLogOffUrl);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}